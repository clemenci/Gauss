# Import the necessary modules.
from Configurables import Generation, DeuteronProduction
from Configurables import MinimumBias, Inclusive, Special, SignalPlain
from Configurables import SignalRepeatedHadronization
prod = "Pythia8Production"

# Add Deuteron as minimum bias production tool.
Generation().addTool(MinimumBias)
Generation().MinimumBias.ProductionTool = "DeuteronProduction"
Generation().MinimumBias.addTool(DeuteronProduction)
Generation().MinimumBias.DeuteronProduction.ProductionToolName = prod

# Add Deuteron as inclusive production tool.
Generation().addTool(Inclusive)
Generation().Inclusive.ProductionTool = "DeuteronProduction"
Generation().Inclusive.addTool(DeuteronProduction)
Generation().Inclusive.DeuteronProduction.ProductionToolName = prod

# Add Deuteron as special production tool.
Generation().addTool(Special)
Generation().Special.ProductionTool = "DeuteronProduction"
Generation().Special.addTool(DeuteronProduction)
Generation().Special.DeuteronProduction.ProductionToolName = prod

# Add Deuteron as plain signal production tool.
Generation().addTool(SignalPlain)
Generation().SignalPlain.ProductionTool = "DeuteronProduction"
Generation().SignalPlain.addTool(DeuteronProduction)
Generation().SignalPlain.DeuteronProduction.ProductionToolName = prod

# Add Deuteron as repeated signal hadronization production tool.
Generation().addTool(SignalRepeatedHadronization)
Generation().SignalRepeatedHadronization.ProductionTool = "DeuteronProduction"
Generation().SignalRepeatedHadronization.addTool(DeuteronProduction)
Generation().SignalRepeatedHadronization.DeuteronProduction.ProductionToolName = prod
