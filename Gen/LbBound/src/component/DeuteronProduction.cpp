// Gaudi.
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/SystemOfUnits.h"

// Event.
#include "Event/HepMCEvent.h"

// ROOT.
#include "TF1.h"

// Local.
#include "DeuteronProduction.h"

//-----------------------------------------------------------------------------
// Implementation file for class: DeuteronProduction
//
// 2016-03-18 : Sophie Baker, Philip Ilten
//-----------------------------------------------------------------------------

// Declare the DeuteronProduction tool.
DECLARE_COMPONENT(DeuteronProduction)

//=============================================================================
// Default constructor.
//=============================================================================
DeuteronProduction::DeuteronProduction(const std::string& type,
				       const std::string& name,
				       const IInterface* parent) 
: BoundProduction(type, name, parent), m_rroot(0) {

  // Declare the tool properties.
  declareInterface<IProductionTool>(this);
  declareProperty("Coefficients", m_cs,
		  "List of coefficients for each production channel.");
  declareProperty("IDs", m_ids,
		  "List of particle IDs (deuteron excluded) "
		  "for each production channel.");
  declareProperty("Modes", m_modes,
		  "Mode for each production channel.");
  declareProperty("Norm", m_norm,
		  "Multiplicative normalization, ignore if less than one.");
}

//=============================================================================
// Initialize the bound process tool.
//=============================================================================
StatusCode DeuteronProduction::boundInitialize() {  
  
  // Initialize the random number generator.
  IRndmGenSvc* randSvc(0);
  try {randSvc = svc<IRndmGenSvc>("RndmGenSvc", true);}
  catch (const GaudiException& e) 
    {Exception("Failed to initialize the RndmGenSvc.");}
  StatusCode sc = m_rflat.initialize(randSvc, Rndm::Flat(0.,1.));
  if (!sc.isSuccess())
    {Exception("Failed to initialize the random number generator.");}
  release(randSvc);

  // Set the global ROOT random number generator.
  m_rroot = new GaudiRandomForROOT(randSvc, sc);
  if (!sc.isSuccess()) 
    {Exception("Failed to initialize GaudiRandomForROOT.");}
  
  // Set the default configuration from arXiv:1504.07242 if needed.
  if (m_cs.size() == 0) {

    // Initialize the eight production channels, table I.
    m_cs    = std::vector< std::vector<double> >(8);
    m_ids   = std::vector< std::vector<double> >(8);
    m_modes = std::vector< int>(8);
    m_ms    = std::vector< std::vector<double> >(8);

    // p n -> gamma d, table II.
    m_modes[0] = 1;
    m_ids[0] = std::vector<double>(3, 2212);
    m_ids[0][1] = 2112; m_ids[0][2] = 22;
    m_cs[0] = std::vector<double>(15);
    m_cs[0][0]  = 1.28;                m_cs[0][8]  = -1723683.9119787284;
    m_cs[0][1]  = 2.3034605532591175;  m_cs[0][9]  = 1679348.7891145353; 
    m_cs[0][2]  = -93.663463313902028; m_cs[0][10] = -1019888.5470232342;
    m_cs[0][3]  = 2565.3904680353621;  m_cs[0][11] = 349840.35161061864; 
    m_cs[0][4]  = -25594.100560137995; m_cs[0][12] = -51662.760038375141;
    m_cs[0][5]  = 143513.10872427333;  m_cs[0][13] = -5.1885266705385051;
    m_cs[0][6]  = -503572.89020794741; m_cs[0][14] = 2.9195632726211609; 
    m_cs[0][7]  = 1149248.0196165806;

    // p n -> pi0 d, table III (unique initial state for first coefficient).
    m_modes[1] = 2;
    m_ids[1] = std::vector<double>(3, 2212);
    m_ids[1][1] = 2112; m_ids[1][2] = 111;
    m_cs[1] = std::vector<double>(5);
    m_cs[1][0] = 85;   m_cs[1][2] = 1.77; m_cs[1][4] = 0.096;
    m_cs[1][1] = 1.34; m_cs[1][3] = 0.38;

    // p n -> pi0 pi0 d, table IV.
    m_modes[2] = 3;
    m_ids[2] = std::vector<double>(4, 2212);
    m_ids[2][1] = 2112; m_ids[2][2] = 111; m_ids[2][3] = 111;
    m_cs[2] = std::vector<double>(5);
    m_cs[2][0] = 2.85519622e+06; m_cs[2][3] = 5.57220777e+00;
    m_cs[2][1] = 1.31114126e+01; m_cs[2][4] = 1.46051932e+06;
    m_cs[2][2] = 2.96145497e+03;
    
    // p n -> pi+ pi- d, table V.
    m_modes[3] = 3;
    m_ids[3] = std::vector<double>(4, 2212);
    m_ids[3][1] = 2112; m_ids[3][2] = 211; m_ids[3][3] = -211;
    m_cs[3] = std::vector<double>(10);
    m_cs[3][0] = 6.46455516e+06; m_cs[3][5] = 2.54935423e+15;
    m_cs[3][1] = 1.05136338e+01; m_cs[3][6] = 1.65669163e+01;
    m_cs[3][2] = 1.97924778e+03; m_cs[3][7] = 2.32961298e+07;
    m_cs[3][3] = 5.36301369e+00; m_cs[3][8] = 1.11937373e+01;
    m_cs[3][4] = 6.04534114e+05; m_cs[3][9] = 2.86815089e+16;

    // p p -> pi+ d, table III.
    m_modes[4] = 2;
    m_ids[4] = std::vector<double>(3, 2212);
    m_ids[4][2] = 211;
    m_cs[4] = m_cs[1];
    m_cs[4][0] *= 2;
      
    // p p -> pi+ pi0 d, table VI.
    m_modes[5] = 3;
    m_ids[5] = std::vector<double>(4, 2212);
    m_ids[5][2] = 211; m_ids[5][3] = 111;
    m_cs[5] = std::vector<double>(5);
    m_cs[5][0] = 5.09870846e+15; m_cs[5][3] = 1.13304315e+01;
    m_cs[5][1] = 1.65581228e+01; m_cs[5][4] = 2.86815089e+16;
    m_cs[5][2] = 2.33337076e+07;

    // n n -> pi- d, table III.
    m_modes[6] = 2;
    m_ids[6] = std::vector<double>(3, 2112);
    m_ids[6][2] = -211;
    m_cs[6] = m_cs[1];
    m_cs[6][0] *= 2;
      
    // n n -> pi- pi0 d, table VI.
    m_modes[7] = 3;
    m_ids[7] = std::vector<double>(4, 2112);
    m_ids[7][2] = -211; m_ids[7][3] = 111;
    m_cs[7] = m_cs[5];
  }
  
  // Initialize the particle property service.
  m_pps = svc<LHCb::IParticlePropertySvc>("LHCb::ParticlePropertySvc", true);

  // Check the configuration.
  if (m_cs.size() != m_ids.size() || m_cs.size() != m_ids.size() ||
      m_cs.size() != m_modes.size())
    {Exception("Coefficient, ID, and mode lists have the same length.");}
  for (int chn = 0; chn < (int)m_cs.size(); ++chn) {
    if (m_ids[chn].size() < 3)
      {Exception("Channels must have 3 or more IDs.");}
    if (m_modes[chn] == 0 && m_cs[chn].size() != 2)
      {Exception("Mode 0 channels must have 2 coefficients.");}
    if (m_modes[chn] == 1 && m_cs[chn].size() != 15)
      {Exception("Mode 1 channels must have 15 coefficients.");}
    if (m_modes[chn] == 2 && m_cs[chn].size() != 5)
      {Exception("Mode 2 channels must have 5 coefficients.");}
    if (m_modes[chn] == 3 && m_cs[chn].size()%5 != 0)
      {Exception("Mode 3 channels must have a multiple of 5 coefficients.");}

    // Always require proton first.
    if (m_ids[chn][1] == 2212)
      {m_ids[chn][1] = m_ids[chn][0]; m_ids[chn][0] = 2212;}
    
    // Append deuteron to the IDs and initialize the masses.
    m_ids[chn].push_back(1000010020);
    for (int id = 0; id < (int)m_ids[chn].size(); ++id) {m_ms[chn].
	push_back(m_pps->find(LHCb::ParticleID(m_ids[chn][id]))->mass());}
  }

  // Set the charged pion mass and number of bound deuterons.
  m_mpi = m_pps->find(LHCb::ParticleID(211))->mass();
  m_nd  = 0;
  
  // Find channel maxima and set the normalization.
  always() << "Initializing deuteron production channels:\n\n";
  double max(0), k, s;
  for (int chn = 0; chn < (int)m_cs.size(); ++chn) {
    TF1 fnc("dsigma", this, &DeuteronProduction::eval, 1e2, 5e3, 1);
    fnc.SetParameter(0, chn);
    k = fnc.GetMaximumX();
    s = fnc.Eval(k);
    always() << "     " << std::scientific << std::setprecision(3) << k
	     << " " << std::scientific << std::setprecision(3) << s;
    for (int id = 0; id < 2; ++id) {always() << " " << m_pps->
	find(LHCb::ParticleID(m_ids[chn][id]))->name();}
    always() << " ->";
    for (int id = 2; id < (int)m_ids[chn].size(); ++id) {always() << " " <<
	m_pps->find(LHCb::ParticleID(m_ids[chn][id]))->name();}
    if (s > max) {max = s;}
    always() << "\n";
  }
  if (m_norm < 1) {m_norm = max;}
  else {m_norm *= max;}
  always() << "\n     Select using maximum cross-section of " << std::scientific
	   << std::setprecision(3) << m_norm << ".\n" << endmsg;
  return sc;
}

//=============================================================================
// Finalize the bound process tool.
//=============================================================================
StatusCode DeuteronProduction::boundFinalize() {
  always() << std::setw(10) << m_nd  << " deuterons bound." << endmsg;
  always() << std::setw(10) << m_nad << " anti-deuterons bound." << endmsg;
  if (m_rroot) delete m_rroot;
  return m_rflat.finalize();
}

//=============================================================================
// Bind the states.
//=============================================================================
StatusCode DeuteronProduction::bindStates(HepMC::GenEvent *event) {
  
  // Create nucleon and anti-nucleon vectors.
  std::vector<HepMC::GenParticle*> nucs, anucs;
  for (HepMC::GenEvent::particle_iterator prt = event->particles_begin();
       prt != event->particles_end(); ++prt) {
    if ((*prt)->end_vertex()) continue;
    if ((*prt)->pdg_id() ==  2212 || (*prt)->pdg_id() ==  2112) { 
      nucs.push_back(*prt);}
    if ((*prt)->pdg_id() == -2212 || (*prt)->pdg_id() == -2112) {
      anucs.push_back(*prt);}
  }

  // Create the nucleon/anti-nucleon combinations and shuffle.
  std::vector<std::pair<HepMC::GenParticle*, HepMC::GenParticle*> > cmbs, acmbs;
  buildCombos(nucs, cmbs);
  buildCombos(anucs, acmbs);

  // Bind the combinations and return.
  bindCombos(event, cmbs);
  bindCombos(event, acmbs);
  return StatusCode::SUCCESS;
}

//=============================================================================
// Build the nucleon-pair combinations and shuffle.
//=============================================================================
void DeuteronProduction::buildCombos(std::vector<HepMC::GenParticle*> &prts,
				     std::vector<std::pair<HepMC::GenParticle*,
				     HepMC::GenParticle*> > &cmbs) {

  // Create the combos.
  for (int prt0 = 0; prt0 < (int)prts.size(); ++prt0) {
    for (int prt1 = prt0 + 1; prt1 < (int)prts.size(); ++prt1) {
      if (abs(prts[prt0]->pdg_id()) == 2112)
	{cmbs.push_back(std::make_pair(prts[prt1], prts[prt0]));}
      else {cmbs.push_back(std::make_pair(prts[prt0], prts[prt1]));}
    }
  }

  // Shuffle.
  for (int idx = (int)cmbs.size() - 1; idx > 0; --idx)
    {swap(cmbs[idx], cmbs[gRandom->Rndm()*(idx + 1)]);}
}

//=============================================================================
// Bind the nucleon-pair combinations.
//=============================================================================
void DeuteronProduction::bindCombos(HepMC::GenEvent *event,
				    std::vector<std::pair<HepMC::GenParticle*,
				    HepMC::GenParticle*> > &cmbs) {
  
  // Cross-section for each production channel.
  std::vector<double> sigmas(m_cs.size(), 0);
  
  // Loop over the nucleon pairs.
  for (std::vector<std::pair<HepMC::GenParticle*, HepMC::GenParticle*> >::
	 iterator cmb = cmbs.begin(); cmb != cmbs.end(); ++cmb) {
    
    // Skip if the pair has already been bound.
    if (cmb->first->end_vertex() || cmb->second->end_vertex()) continue;

    // Calculate the momentum difference.
    const HepMC::FourVector &fv0 = cmb->first->momentum();
    const HepMC::FourVector &fv1 = cmb->second->momentum();
    TLorentzVector lv0(fv0.px(), fv0.py(), fv0.pz(), fv0.e());
    TLorentzVector lv1(fv1.px(), fv1.py(), fv1.pz(), fv1.e());
    TLorentzVector lv01(lv0 + lv1);
    lv0.Boost(-lv01.BoostVector());
    lv1.Boost(-lv01.BoostVector());
    double k((lv0 - lv1).P()); 
    
    // Try binding each channel.
    double sum(0);
    for (int chn = 0; chn < (int)sigmas.size(); ++chn) {
      if (abs(cmb->first->pdg_id()) == m_ids[chn][0] &&
	  abs(cmb->second->pdg_id()) == m_ids[chn][1])
	{sigmas[chn] = sigma(k, chn);}
      else {sigmas[chn] = 0; continue;}
      if (sigmas[chn] > m_norm) {Warning("Maximum weight exceeded.");}
      if (m_rflat() >= sigmas[chn]/m_norm) {sigmas[chn] = 0;}
      sum += sigmas[chn];
    }
    
    // Pick a bound channel.
    if (sum == 0) continue;
    double rndm(sum*m_rflat()); int chn(-1);
    do rndm -= sigmas[++chn];
    while (rndm > 0. && chn < (int)sigmas.size());
    chn = sigmas.size() ? 0 : chn;
    
    // Generate the decay and add to the event record.
    m_psgen.SetDecay(lv01, m_ids[chn].size() - 2, &m_ms[chn][2]);
    m_psgen.Generate();
    HepMC::GenVertex* vertex = new HepMC::GenVertex();
    event->add_vertex(vertex);
    vertex->add_particle_in(cmb->first);
    vertex->add_particle_in(cmb->second);
    for(int dtr = 0; dtr < m_psgen.GetNt(); ++dtr) {
      TLorentzVector* lv(m_psgen.GetDecay(dtr));
      HepMC::FourVector fv(lv->Px(), lv->Py(), lv->Pz(), lv->E());
      int id = m_ids[chn][dtr + 2];
      if (cmb->first->pdg_id() < 0 &&
	  !m_pps->find(LHCb::ParticleID(id))->selfcc()) {id *= -1;}
      vertex->add_particle_out
	(new HepMC::GenParticle(fv, id, LHCb::HepMCEvent::StableInProdGen,
				0, 0));
    }
    cmb->first->set_status(LHCb::HepMCEvent::DecayedByProdGen);
    cmb->second->set_status(LHCb::HepMCEvent::DecayedByProdGen);
    cmb->first->pdg_id() > 0 ? ++m_nd : ++m_nad;
  }
}

//=============================================================================
// Single pion final state fit, equations 10/13/14 of arXiv:1504.07242.
//=============================================================================
double DeuteronProduction::fit(double k, std::vector<double> &c, int i) {
  return c[i]*pow(k, c[i + 1])/(pow((c[i + 2] - exp(c[i + 3]*k)),2) + c[i + 4]);
}

//=============================================================================
// Function to evaluate for TF1 maximizing and integration.
//=============================================================================
double DeuteronProduction::eval(double *x, double *p) {
  return sigma(x[0], p[0]);
}

//=============================================================================
// Return the cross-section for a given channel.
//=============================================================================
double DeuteronProduction::sigma(double k, int chn) {
  double sum(0);
  int mode(m_modes[chn]);
  std::vector<double> &c = m_cs[chn];
  std::vector<double> &m = m_ms[chn];

  // Check allowed phase-space and convert k to MeV.
  double ecm(sqrt(m[0]*m[0] + k*k/4) + sqrt(m[1]*m[1] + k*k/4)), mtot(0);
  k /= Gaudi::Units::GeV;
  for (int dtr = 2; dtr < (int)m.size(); ++dtr) mtot += m[dtr];
  if (ecm < mtot) {
    sum = 0;

  // Step function, e.g. coalescence model.
  } else if (mode == 0) {
    sum = k < c[0] ? c[1] : 0;
  
  // p n -> gamma d, equation 7 where first parameter is function k-split.
  } else if (mode == 1) {
    if (k < c[0]) {for (int i = 1; i < 13; ++i) {sum += c[i]*pow(k, i - 2);}
    } else {sum = exp(-c[13]*k - c[14]*k*k);}

  // p/n p/n -> pi d, equation 10.
  } else if (mode == 2)  {
    double s(ecm*ecm), q(sqrt(pow(s + m[2]*m[2] - m.back()*m.back(), 2)
			      /(4*s) - m[2]*m[2]));
    sum = fit(q/m_mpi, c, 0);

  // p/n p/n -> pi pi d, equations 13 and 14.
  } else if (mode == 3) {
    for (int i = 0; i < (int)c.size(); i += 5) sum += fit(k, c, i);
  }
  return sum;
}

//=============================================================================
// The END.
//=============================================================================
