#include "EvtGenModels/EvtModelRegExtras.hh"

#include "EvtGenModels/EvtBToKStarllDurham07.hh"
#include "EvtGenModels/EvtDTohhhh.hh"
#include "EvtGenModels/EvtXLL.hh"
#include "EvtGenModels/EvtLbAmpGen.hh"
#include "EvtGenModels/EvtBnoCB0to4piCP.hh"

//we use unique_ptr here to show explicit transfer of ownership
std::unique_ptr<const EvtModelList> EvtModelRegExtras::getModels() {

        EvtModelList* models { new EvtModelList{} };
	if (models == nullptr) {
	    return std::unique_ptr<const EvtModelList>{};
	}

	models->push_back(new EvtBToKStarllDurham07());
        models->push_back( new EvtDTohhhh());
	models->push_back( new EvtXLL());
        models->push_back( new EvtLbAmpGen());
        models->push_back( new EvtBnoCB0to4piCP());
        return std::unique_ptr<const EvtModelList>(models);

}
