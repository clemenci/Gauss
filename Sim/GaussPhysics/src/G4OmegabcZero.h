#ifndef G4OmegabcZero_h
#define G4OmegabcZero_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         OmegabcZero                        ###
// ######################################################################

class G4OmegabcZero : public G4ParticleDefinition
{
 private:
  static G4OmegabcZero * theInstance ;
  G4OmegabcZero( ) { }
  ~G4OmegabcZero( ) { }


 public:
  static G4OmegabcZero * Definition() ;
  static G4OmegabcZero * OmegabcZeroDefinition() ;
  static G4OmegabcZero * OmegabcZero() ;
};


#endif
