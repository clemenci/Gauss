#-- Options to generate events with a fiex number of interaction
from Configurables import Generation
gaussGen = Generation("Generation")
gaussGen.PileUpTool = "FixedNInteractions"

#-- The default is set to 1, uncomment and change appropriately the  
#-- following lines when needed
## gaussGen.addTool( FixedNInteractions )
## gaussGen.FixedNInteractions.NInteractions = 2
